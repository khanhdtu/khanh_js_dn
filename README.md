CSS for [ loading ... ]
@keyframes dotdotdot{
    0% {content : '...'}
    25% {content : ''}
    50% {content : '.'}
    75% {content : '..'}
}
.dotdotdot:after{
    font-weight : 300px;
    content : '...';
    display : inline-block;
    width : 20px;
    text-align : left;
    animation : dotdotdot 1.5s linear infinity;
}